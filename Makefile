MINIKUBE_PROFILE := nats-cluster-balancing

up:
	minikube -p $(MINIKUBE_PROFILE) start
	helmfile apply

down:
	minikube -p $(MINIKUBE_PROFILE) stop

delete:
	minikube -p $(MINIKUBE_PROFILE) delete