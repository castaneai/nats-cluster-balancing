# nats-cluster-balancing

You can check the connection distribution to NATS clusters through Kubernetes service.

## Requirements

- minikube
- kubectl
- helm
- helmfile

```bash
make up
kubectl apply -f https://k8s.io/examples/admin/dns/dnsutils.yaml
kubectl exec -it dnsutils -- bash

apt update -y
apt install -y telnet curl jq
for x in `seq 1 100`; do telnet my-nats 4222 &> /dev/null & done
# show num_connections per host
for host in `dig +short my-nats.default.svc.cluster.local.`; do echo -n "${host}: "; curl -s "http://${host}:8222/connz" | jq .num_connections; done
# example output:
# 172.17.0.2: 25
# 172.17.0.4: 35
# 172.17.0.3: 40
```
